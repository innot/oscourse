/* See COPYRIGHT for copyright information. */

#include <inc/x86.h>
#include <kern/kclock.h>
#include <inc/stdio.h>
#include <kern/picirq.h>

const uint64_t max_clock_cycle = 250000; // 1/16 seconds
// const uint64_t min_clock_cycle = 488;
struct Time2Freq converter[14] = {
	{0x2, 0}, // this one is not used
	{0x3, 122},
	{0x4, 244},
	{0x5, 488},
	{0x6, 976},
	{0x7, 1953},
	{0x8, 3906},
	{0x9, 7812},
	{0xA, 15625},
	{0xB, 31250},
	{0xC, 62500},
	{0xD, 125000},
	{0xE, 250000},
	{0xF, 500000}
};
unsigned default_timer_rate_id = 11;

// sets clock interrupt rate 
uint64_t 
rtc_set_int_rate(uint64_t time)
{
	uint32_t i;
	uint8_t next_freq = 0;
	for (i = 1; (i < sizeof(converter)) && (converter[i].time <= max_clock_cycle); i++) {
		if (time > converter[i-1].time && time <= converter[i].time) {
			next_freq = converter[i].freq;
			break;
		}
	}
	if (converter[i].time > max_clock_cycle) { // time > max_clock_cycle
		next_freq = converter[i - 1].freq;
		i--;
	}
	
	// // critical section ?
	// nmi_disable();

	// outb(IO_RTC_CMND, RTC_AREG);
	// uint8_t A = inb(IO_RTC_DATA);
	// // cprintf("A clock register: %08x\n", A);
	// A >>= 4;
	// A <<= 4;
	// A |= next_freq;
	// outb(IO_RTC_DATA, A);
	
	// nmi_enable();

	return converter[i].time;
}

void 
turn_on_the_oscillator(void)
{
	nmi_disable();
	pic_send_eoi(rtc_check_status());

	outb(IO_RTC_CMND, RTC_AREG);
	uint8_t A = inb(IO_RTC_DATA);
	A &= 0x8F; // clear 4-6 bits
	A |= 0x20; // set them to 010
	outb(IO_RTC_DATA, A);
	
	nmi_enable();
}

void 
turn_off_the_oscillator(void)
{
	nmi_disable();

	outb(IO_RTC_CMND, RTC_AREG);
	uint8_t A = inb(IO_RTC_DATA);
	A &= 0x8F; // clear 4-6 bits
	// A |= 0x00; // set them to 110 (or zeroes?)
	outb(IO_RTC_DATA, A);
	
	nmi_enable();
}

void
rtc_init(void)
{
	nmi_disable();
	// LAB 4: your code here
	outb(IO_RTC_CMND, RTC_BREG);
	uint8_t B = inb(IO_RTC_DATA);
	B |= RTC_PIE;
	outb(IO_RTC_DATA, B);

	outb(IO_RTC_CMND, RTC_AREG);
	uint8_t A = inb(IO_RTC_DATA);
	A &= 0xF0;
	A |= converter[default_timer_rate_id].freq;
	outb(IO_RTC_DATA, A);
	
	clock_tsc = read_tsc();
	last_interrupt_timing = converter[default_timer_rate_id].time;
	nmi_enable();
}

uint8_t
rtc_check_status(void)
{
	uint8_t status = 0;
	// LAB 4: your code here
	outb(IO_RTC_CMND, RTC_CREG);
	status = inb(IO_RTC_DATA);
	return status;
}


unsigned
mc146818_read(unsigned reg)
{
	outb(IO_RTC_CMND, reg);
	return inb(IO_RTC_DATA);
}

void
mc146818_write(unsigned reg, unsigned datum)
{
	outb(IO_RTC_CMND, reg);
	outb(IO_RTC_DATA, datum);
}

