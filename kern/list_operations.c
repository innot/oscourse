#include <kern/alloc.h>
// #include <inc/stdio.h>
#include <inc/assert.h>

#include <kern/sched-head.h>

ListElement *
list_find(ListElement *list, bool (*cmp)(ListElement *a)) 
{
	ListElement *i = list;
	while (i) {
		if (cmp(i))
			return i;
		else
			i = i->next;
	}
	return NULL;
}

ListElement *
list_find_element(ListElement *list, struct Env *what)
{
	ListElement *i = list;
	while (i) {
		if (i->env == what)
			return i;
		else
			i = i->next;
	}
	return NULL;
}

ListElement *
list_find_extreme(ListElement *list, bool (*better)(ListElement *cur, ListElement *extr), bool runnable_only) 
{
	ListElement *i = list, *extr = list;
	if (!list)
		return NULL;
	while ((i = i->next)) {
		if (!runnable_only || is_runnable(i))
			if (better(i, extr))
				extr = i;
	}
	return extr;
}

ListElement *
list_find_border(ListElement *start, ListElement *end, int64_t (*diff)(ListElement *a, ListElement *b))
{
	int max_step = 0;//, max_step_2 = 0;
	ListElement *max_step_el = start;//, max_step_el_2 = start;
	if (start == end)
		return end;
	ListElement *i = start;
	while ((end && (i != end)) || (!end && i->next)) {
		int delta = diff(i->next, i);
		if (delta > max_step + FAST_PROC_STEP_EPS) {
			max_step = delta;
			max_step_el = i;
		}
		i = i->next;
	}
	if (!max_step) // all elements are almost equal
		max_step_el = i;
	return max_step_el;
}

ListElement *
push_before_list(ListElement **list_beginning, struct Env *what, unsigned *list_len)
{
	if (!list_beginning) {
		log_printf(0, "no actual list passed to push_before_list()\n");
		return NULL;
	}
	if (!list_len) {
		log_printf(0, "no length ref passed to push_before_list()\n");
		return NULL;
	}
	ListElement *result = test_alloc(sizeof(ListElement));
	if (!result) {
		log_printf(2, "memory allocation failed in push_before_list()\n");
		return NULL;
	}
	result->next = *list_beginning;
	result->prev = NULL;
	result->env = what;
	if (*list_beginning)
		(*list_beginning)->prev = result;
	*list_beginning = result;
	(*list_len)++;
	return result;
}

ListElement *
push_after_list(ListElement **list_ending, struct Env *what, unsigned *list_len)
{
	if (!list_ending) {
		log_printf(0, "no actual list passed to push_after_list()\n");
		return NULL;
	}
	if (!list_len) {
		log_printf(0, "no length ref passed to push_after_list()\n");
		return NULL;
	}
	ListElement *result = test_alloc(sizeof(ListElement));
	if (!result) {
		log_printf(2, "allocation failed in push_after_list()\n");
		return NULL;
	}
	result->next = NULL;
	result->prev = *list_ending;
	result->env = what;
	if (*list_ending)
		(*list_ending)->next = result;
	else // the destination list seems to be free
		*list_ending = result;
	(*list_len)++;
	return result;
}

ListElement *
make_list(ListElement *beginning, ListElement *ending, unsigned *list_len, bool fast) 
{
	if (!beginning)
		return NULL;
	if (!list_len) {
		log_printf(0, "no length ref passed to make_list()\n");
		return NULL;
	}
	ListElement *i = beginning, *result = NULL, *end;
	end = push_before_list(&result, i->env, list_len);
	if (fast)
		i->env->fast = true;
	else 
		i->env->fast = false;
	// (*list_len) = 1;
	if (beginning == ending)
		return result;
	while ((i = i->next) && (!ending || i != ending->next)) {
		end = push_after_list(&end, i->env, list_len);
		if (fast)
			i->env->fast = true;
		else 
			i->env->fast = false;
		// (*list_len)++;
	}
	return result;
}

int 
free_list(ListElement *beginning, unsigned *list_len)
{
	if (!beginning)
		return -1;
	ListElement *i = beginning;
	while (i) {
		ListElement *tmp = i->next;
		remove_from_list(&beginning, i, list_len);
		i = tmp;
	}
	// if (list_len) {
	// 	(*list_len) = 0;
	// }
	return 0;
}

int 
remove_from_list(ListElement **list_beginning, ListElement *what, unsigned *list_len) 
{
	if (!list_beginning || !(*list_beginning)) {
		log_printf(0, "no actual list passed to remove_from_list()\n");
		return -1;
	}
	if (!list_len) {
		log_printf(0, "no length ref passed to remove_from_list()\n");
		return -1;
	}
	if (what != *list_beginning) // this also means that what->prev != NULL
			what->prev->next = what->next; 
	else // we are removing the list beginning
		*list_beginning = what->next;
	if (what->next)
		what->next->prev = what->prev;
	test_free(what);
	(*list_len)--;
	return 0;
}

int 
output_list(int level, ListElement *list)
{
	if (level < LOG_LEVEL)
		return 1;
	if (!list) {
		log_printf(0, "free list passed to output_list()\n");
		return -1;
	}
	ListElement *i = list;
	while (i) {
		// ListElement *tmp = i->next;
		// remove_from_list(&beginning, i);
		cprintf("%u: w=%u ", ENVX(i->env->env_id), i->env->time_waiting);
		cprintf("a=%u ", i->env->assumed_time);
		cprintf("m=%u ", (i->env->default_timing ? 1 : 0));
		cprintf("t_a=%u ", i->env->total_time_active);
		cprintf("c_a=%u\n", i->env->time_active);
		i = i->next;
	}
	return 0;
}

// int
// list_length(ListElement *list, bool runnable_only)
// {
// 	int result = 0;
// 	while (list) {
// 		if (!runnable_only || is_runnable(list))
// 			result++;
// 		list = list->next;
// 	}
// 	return result;
// }

inline bool 
is_runnable(ListElement *what)
{
	return what->env->env_status == ENV_RUNNABLE || what->env->env_status == ENV_RUNNING;
}

/*
 * The algorithm used below is Mergesort, because that works really well
 * on linked lists, without requiring the O(N) extra space it needs
 * when you do it on arrays.
 */

/*
 * The following code is copyrighted 
 * 2001 Simon Tatham.
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL SIMON TATHAM BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

ListElement *
list_sort(ListElement *list, int64_t (*cmp)(ListElement *a, ListElement *b)) 
{
	ListElement *p, *q, *e, *tail;
	int insize, nmerges, psize, qsize, i;

	
	// Silly special case: if `list' was passed in as NULL, return
	// NULL immediately.
	 
	if (!list)
		return NULL;

	insize = 1;

	while (true) {
		p = list;
		list = NULL;
		tail = NULL;

		nmerges = 0;  /* count number of merges we do in this pass */

		while (p) {
			nmerges++;  /* there exists a merge to be done */
			/* step `insize' places along from p */
			q = p;
			psize = 0;
			for (i = 0; i < insize; i++) {
				psize++;
				q = q->next;
				if (!q) break;
			}

			/* if q hasn't fallen off end, we have two lists to merge */
			qsize = insize;

			/* now we have two lists; merge them */
			while (psize > 0 || (qsize > 0 && q)) {
				/* decide whether next ListElement of merge comes from p or q */
				if (psize == 0) {
					/* p is empty; e must come from q. */
					e = q; 
					q = q->next; 
					qsize--;
				} else if (qsize == 0 || !q) {
					/* q is empty; e must come from p. */
					e = p; 
					p = p->next; 
					psize--;
				} else if (cmp(p,q) <= 0) {
					/* First ListElement of p is lower (or same);
					 * e must come from p. */
					e = p; 
					p = p->next; 
					psize--;
				} else {
					/* First ListElement of q is lower; e must come from q. */
					e = q; 
					q = q->next; 
					qsize--;
				}

				/* add the next ListElement to the merged list */
				if (tail) {
					tail->next = e;
				} else {
					list = e;
				}
				e->prev = tail;
				tail = e;
			}

			/* now p has stepped `insize' places along, and q has too */
			p = q;
		}
		tail->next = NULL;

		/* If we have done only one merge, we're finished. */
		if (nmerges <= 1)   /* allow for nmerges==0, the empty list case */
			return list;

		/* Otherwise repeat, merging lists twice the size */
		insize *= 2;
	}
}