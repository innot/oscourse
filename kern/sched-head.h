#ifndef JOS_KERN_SCHED_HEAD_H
#define JOS_KERN_SCHED_HEAD_H

#include <inc/env.h>

// shall envs travel between lists in the middle of phase?
#define FTSTF

struct ListElement {
	// Handles list operations for struct Env
	struct ListElement *next;
	struct ListElement *prev;
	struct Env *env;
};
typedef struct ListElement ListElement;

extern ListElement *loaded_envs;
extern ListElement *fast_envs;
extern ListElement *slow_envs;

extern unsigned loaded_envs_len, fast_envs_len, slow_envs_len;
extern uint64_t current_phase_time;
extern bool cur_has_finished;
extern uint64_t cur_env_wants_t;

#define MAX_ALLOWED_DELTA_PERCENT 0.1
	// ^ manually-handled processes must not exceed their assumed time 
	// ^ for more than this if they don't want to forfeit
#define MAX_WAITING_TIME 10000000 
	// ^ each process must be run at least this often 
	// ^ 20 s
#define PHASE_TIME (0.9 * MAX_WAITING_TIME) 
#define AVG_CLOCK_MEASUREMENTS_N 6
	// ^ how many measurements scheduler must take to decide about processor frequency
#define TIME_BETWEEN_AMNESTIES 60000000 // 1 minute
#define AMNESTY_POINTS 2 // penalty will decrease for this every TIME_BETWEEN_AMNESTIES
#define MAX_PENALTY 2 // for blocking
#define MIN_SUSPICIOUS_TIMING 122000 // value for the second banning condition
#define WEIGHTING_FACTOR 0.1 // for automatic assuming
#define DEF_ASSUMED_TIME (100 * 62500) 
	// ^ default time initially set for each process
#define MAX_FAST_PROC_REQ_T 250000
	// ^ processes with assumed time greater then this 
	// ^ are considered slow by default if others are present
#define FAST_PROC_STEP_EPS 10000
	// ^ if step in list_find_border() is greater then the maximum one
	// ^ but for less then this, it's skipped
#define SLOW_PROC_EMBEDDED_STEPS 5

ListElement *push_before_list(ListElement **list_beginning, struct Env *what, unsigned *list_len);
ListElement *push_after_list(ListElement **list_ending, struct Env *what, unsigned *list_len);
int remove_from_list(ListElement **list_beginning, ListElement *what, unsigned *list_len);
ListElement *list_sort(ListElement *list, int64_t (*cmp)(ListElement *a, ListElement *b));
	// ^ sorts the list according to cmp()
	// ^ merge sort is used
ListElement *list_find(ListElement *list, bool (*cmp)(ListElement *a));
	// ^ finds the first element for which cmp(e) == true
	// ^ returns NULL if there is no such element
ListElement *list_find_border(ListElement *start, ListElement *end, int64_t (*cmp)(ListElement *a, ListElement *b));
	// ^ finds the part of the list which will conform to fast processes
	// ^ it's always [start, retval]
ListElement *list_find_extreme(ListElement *list, bool (*better)(ListElement *cur, ListElement *extr), bool runnable_only);
	// ^ return the last element for which better() returns true
	// ^ used for finding maximums/minimums
ListElement *make_list(ListElement *beginning, ListElement *ending, unsigned *list_len, bool fast);
	// ^ create list from another list's part
int free_list(ListElement *beginning, unsigned *list_len);
int output_list(int log_level, ListElement *list);
// int list_length(ListElement *list, bool runnable_only);
inline bool is_runnable(ListElement *what);
ListElement *list_find_element(ListElement *list, struct Env *what);

void run_env(struct Env *env);
int make_env_lists();
void setup_cpu_freq();
void deal_with_curenv(uint64_t time_spent);
void add_waiting_time(uint64_t time_spent);
#endif	// !JOS_KERN_SCHED_HEAD_H
