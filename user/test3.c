#include <inc/lib.h>
#include <inc/x86.h>
#include <inc/random.h>

void
umain( int argc, char **argv )
{
	int i, j;
	if (sys_env_set_time(3000)) // 0.001 seconds
		cprintf("error");
	sys_yield();
	for( j = 500; j > 0; j--) {
		cprintf("%d bottles of beer on the wall\n", j);
		cprintf("%d bottles of beer!\n", j);
		cprintf("take one, pass it around\n");
	}
	cprintf("and everyone ready");
}
